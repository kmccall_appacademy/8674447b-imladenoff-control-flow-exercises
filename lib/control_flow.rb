# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  while /[a-z]/.match(str)
    str.delete!(/[a-z]/.match(str).to_s)
  end

  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  half_str = str.length / 2
  if str.length.even?
    str[half_str - 1..half_str]
  else
    str[half_str]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  count = 1
  working = 1
  until count > num
    working *= count
    count += 1
  end
  working
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each_with_index do |word, index|
    unless index == arr.length - 1
      str << word + separator
    else
      str << word
    end
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str2 = ""
  str.each_char.with_index do |char, index|
    if index.even?
      str2[index] = str[index].downcase
    else
      str2[index] = str[index].upcase
    end
  end
  str2
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split
  arr.each.with_index do |word, index|
    if word.length > 4
      arr[index] = word.reverse
    end
  end
  arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a

  arr.each.with_index do |num, index|
    if num % 15 == 0
      arr[index] = "fizzbuzz"
    elsif num % 3 == 0
      arr[index] = "fizz"
    elsif num % 5 == 0
      arr[index] = "buzz"
    end
  end

  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr2 = []

  arr.each.with_index do |item, index|
    arr2[arr.length - 1 - index] = item
  end

  arr2
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2

  working = 2

  while working < num
    if num % working == 0
      return false
    end
    working += 1
  end

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  counter = 1
  arr = []
  while counter <= num
    arr << counter if num % counter == 0
    counter += 1
  end
  arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)
  prime_factors = factors.select {|k| prime?(k)}

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = []; odd = []

  arr.each do |num|
    num.even? ? even << num : odd  << num

    if odd.length == 2
      arr.each { |k| return k if k.even?}
    elsif even.length == 2
      arr.each { |k| return k if k.odd?}
    end
    
  end
end
